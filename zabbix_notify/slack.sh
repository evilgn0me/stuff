#!/bin/bash

URL='https://hooks.slack.com/services/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'

TO=$1
SUBJ=$2
MSG=$3

payload="payload={\"channel\": \"${TO}\", \"text\": \"${SUBJ}\" }"

#test to check you payload
#echo "${payload}" > ~/slack.test
curl -X POST --data-urlencode "${payload}" $URL
