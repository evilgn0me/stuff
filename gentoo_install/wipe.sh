#!/bin/bash

umount /mnt/gentoo/sys
umount /mnt/gentoo/proc
umount /mnt/gentoo/dev
umount /mnt/gentoo/boot
umount /mnt/gentoo

mdadm --stop /dev/md0
mdadm --stop /dev/md1

dd if=/dev/zero of=/dev/sda1 bs=4096 count=2
dd if=/dev/zero of=/dev/sdb1 bs=4096 count=2
dd if=/dev/zero of=/dev/sda2 bs=4096 count=2
dd if=/dev/zero of=/dev/sdb2 bs=4096 count=2

partprobe