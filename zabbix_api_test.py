from zabbix.api import ZabbixAPI
from distutils.version import LooseVersion
# Create ZabbixAPI class instance
zapi = ZabbixAPI(url='http://zabbix.faunus.io', user='admin', password='yosh3Bouqu')

# Get all monitored hosts
result1 = zapi.host.get(monitored_hosts=1, output='extend')

# Get all disabled hosts
result2 = zapi.do_request('host.get',
                          {
                              'filter': {'status': 1},
                              'output': 'extend'
                          })

# Filter results
hostnames1 = [host['host'] for host in result1]
hostnames2 = [host['host'] for host in result2['result']]

print zapi.api_version()

if LooseVersion(zapi.api_version()) <= LooseVersion('3.4.x'):
     print 'fail'
