#!/bin/sh
DATE=$(date +%y-%m-%d)

RDRECP=/etc/postfix/cyclonelabs.com.recipients

LLOGINS=/etc/postfix/cyclonelabs.com.logins
LMBOXES=/etc/postfix/cyclonelabs.com.mailboxes

if [ "$(whoami)" != root ] ; then
    echo "\nPlease run this script as root!\n"
    exit 1
fi

function usage {
	echo "Usage: 

	$0 function filename

	available functions:
	rotterdam - add users to configs at rotterdam server
	london - add users to configs at london server, dovecot bulk file will be create in current directory
	test - use for test and other stuff
	delete - deleting users from configs (works for all servers)

	filename should consist from user emails, one email per line
	you still need to use updateMaps and addList scripts"
	exit 1
}

if [ "$*" == "" ]; then
    usage
    exit 1
fi

if [ ! -f $2 ]; then
	echo "file $2 does not exist"
	exit 1
fi


function rotterdam {
	for EMAIL in $(cat $1)
	do
                CEMAIL=$( echo $EMAIL |tr -d '\r' | tr '[:upper:]' '[:lower:]' | grep -E -o "\b[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+\b" )		
		
		MSG=$(grep $CEMAIL $RDRECP)

		if [ "$MSG" == "" ]
		then
			echo "$CEMAIL  OK" >> $RDRECP
		else
			echo "$CEMAIL already added to $RDRECP"
		fi 

	done
}  

function london {
        for EMAIL in $(cat $1)
        do
                CEMAIL=$( echo $EMAIL |tr -d '\r' |tr '[:upper:]' '[:lower:]' |grep -E -o "\b[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+\b" )

		        MSG=$(grep $CEMAIL $LMBOXES)

                if [ "$MSG" == "" ]
                then
                        echo "$CEMAIL  OK" >> $LMBOXES 
                else
                        echo "$CEMAIL already added to $LMBOXES"
                fi

		MSG=""

		#add user login
		MSG=$(grep $CEMAIL $LLOGINS)

                if [ "$MSG" == "" ]
                then
                        echo "$CEMAIL  $CEMAIL" >> $LLOGINS
                else
                        echo "$CEMAIL already added to $LLOGINS"
                fi

		MSG=""

		#generate dovecot password list
		echo $DATE
		PASS=$(openssl rand -base64 100 | tr -d -c '[:alnum:]' |fold -w 16 |head -n1)

		MSG=$(grep $CEMAIL dovecot-$DATE)
		if [ "$MSG" == "" ]
                then
                        echo "$CEMAIL $PASS" >> dovecot-$DATE
                else
                        echo "$CEMAIL already added to dovecot-$DATE"
                fi
		done
}

function test {
        for EMAIL in $(cat $1)
	do
                CEMAIL=$( echo $EMAIL |tr -d '\r' |tr '[:upper:]' '[:lower:]' | grep -E -o "\b[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+\b" )
		echo $CEMAIL
	done
}


function delete {
		for EMAIL in $(cat $1)
		do
				CEMAIL=$( echo $EMAIL |tr -d '\r' | grep -E -o "\b[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+\b" )

				if [ -f "$RDRECP" ]; then
					cp $RDRECP $RDRECP-save-$DATE
					grep -v $CEMAIL $RDRECP > $RDRECP-tmp
					mv $RDRECP-tmp $RDRECP
				fi

				if [ -f "$LLOGINS" ]; then
					cp $LLOGINS $LLOGINS-save-$DATE
					grep -v $CEMAIL $LLOGINS > $LLOGINS-tmp
					mv $LLOGINS-tmp $LLOGINS
				fi

				if [ -f "$LMBOXES" ]; then
					cp $LMBOXES $LMBOXES-save-$DATE
					grep -v $CEMAIL $LMBOXES > $LMBOXES-tmp
					mv $LMBOXES-tmp $LMBOXES
				fi
		done
}
	

# call arguments verbatim:
$@

