This is ansible playbook to install zabbix on ipfire.
To use this playbook copy zabbix_agent.init and zabbix_agentd.conf to /srv directory on your ansible server.
Also you need to add your ipfire hosts to group routers on your ansible installation.